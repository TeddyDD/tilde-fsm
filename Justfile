srcdir = "addons/tilde-toolkit/fsm/"
test_template = 'extends "res://addons/gut/test.gd"

# #############
# Seutp/Teardown
# #############
func prerun_setup():
	pass

func setup():
	pass

func teardown():
	pass

func postrun_teardown():
	pass

# #############
# Tests
# #############
func test_pending():
	pending()
'


# Line count excluding tests and comments
wc:
	@cat {{srcdir}}/*.gd | sed '/^\s*#/d;/^\s*$/d' | wc -l

# run tests in command line
test:
	godot -d -s addons/gut/gut_cmdln.gd -gdir=res://test -gexit -gignore_pause

# Generate test template
@gentest name: 
	cat <<< '{{test_template}}' > test/test_{{name}}.gd
	
