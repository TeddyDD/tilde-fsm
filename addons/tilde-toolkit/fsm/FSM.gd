extends "res://addons/tilde-toolkit/fsm/StateInterface.gd"

var is_sub_fsm

# Returns ref to states.
signal state_changed(old_state, new_state)

# Name of default state
var default_state = null

# Dict mapping names of states to instances
# "some_state" -> StateInstance
var _states = {}
# Name of current, active state
var _current_state

var state_time

static func new_root(name, rootNode):
	var f = new(name)
	f.root = rootNode
	f.is_sub_fsm = false
	return f

func _init(name):
	self.name = name
	self.is_sub_fsm = true

# `from` is a ref
func enter(from):
	state_time = 0
	assert(not default_state == null)
	_current_state = default_state
	_states[_current_state].enter(from)

func process(delta):
	state_time += delta
	_states[_current_state].process(delta)
	var to = _states[_current_state]._check_transitions()
	if to != null:
		change_state(to)
	if fsm != null:
		to = self._check_transitions()
		if to != null:
			fsm.change_state(to)

func add_state(ref):
	var n = ref.name
	_states[n] = ref
	_states[n].fsm = self
	_states[n].root = root
	if default_state == null:
		default_state = n

# `to` is a name of state
func change_state(to):
	var old_state = _states[_current_state]
	old_state.exit(_states[to])
	_current_state = to
	_states[_current_state].enter(old_state)
	state_time = 0
	emit_signal("state_changed", old_state, _states[_current_state])

# Recursivly get currently active state
# Useful for nested FSMs
func get_active_state():
	return _states[_current_state].get_active_state()
