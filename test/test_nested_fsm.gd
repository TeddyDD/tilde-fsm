extends "res://addons/gut/test.gd"
var u = preload("res://test/utils.gd").new()
var fsm
var innerFsm
var s1
var s2
var s3
var s4
var tTrans

# #############
# Seutp/Teardown
# #############

func setup():
	fsm = u.FSM.new_root("root", null)
	innerFsm = u.FSM.new("inner")
	s1 = u.State.new()
	s1.name = "s1"
	s2 = u.State.new()
	s2.name = "s2"
	s3 = u.State.new()
	s3.name = "s3"
	s4 = u.State.new()
	s4.name = "s4"
	tTrans = u.TrueTransition.new()

func teardown():
	fsm = null
	innerFsm = null
	s1 = null
	s2 = null
	s3 = null
	s4 = null
	tTrans = null


# #############
# Tests
# #############
func test_creating_nested():
	fsm.add_state(s1)
	assert_eq(fsm._states.size(), 1)
	
	fsm.add_state(innerFsm)
	assert_extends( fsm._states["inner"], u.FSM, "added object should be FSM")
	
	fsm._states["inner"].add_state(s2)
	
	assert_has(fsm._states, "s1", "fsm should contain s1 state")
	assert_has(fsm._states, "inner", "fsm should contain nested fsm")
	assert_has(fsm._states["inner"]._states, "s2", "nested fsm should contain s2 state")

# Test following nested FSM
# (s1 -> (s2 -> s3))
func test_entering_nested():
	fsm.add_state(s1)
	fsm.default_state = "s1"
	fsm.add_state(innerFsm)
	innerFsm.add_state(s2)
	innerFsm.add_state(s3)
	innerFsm.default_state = "s2"
	
	s1.add_transition("inner", tTrans)
	s2.add_transition("s3", tTrans)
	
	fsm.enter(null)
	assert_eq(fsm._current_state, 's1')
	
	fsm.process(1)
	assert_eq(fsm._current_state, 'inner')
	assert_eq(fsm._states["inner"]._current_state, 's2')
	
	fsm.process(1)
	assert_eq(fsm._current_state, 'inner')
	assert_eq(fsm._states["inner"]._current_state, 's3')
	
	fsm.process(1)
	assert_eq(fsm._current_state, 'inner')
	assert_eq(fsm._states["inner"]._current_state, 's3')
	
# Test following FSM
# (s1 -> (s2) -> s3
func test_exiting_nested():
	fsm.add_state(s1)
	fsm.default_state = "s1"
	
	fsm.add_state(innerFsm)
	innerFsm.add_state(s2)
	innerFsm.default_state = "s2"
	
	fsm.add_state(s3)
	
	s1.add_transition("inner", tTrans)
	innerFsm.add_transition("s3", tTrans)
	
	fsm.enter(null)
	
	assert_eq(fsm._current_state, 's1')
	
	fsm.process(1)
	assert_eq(fsm._current_state, "inner")
	assert_eq(fsm._states["inner"]._current_state, 's2')
	assert_extends(fsm._states[fsm._current_state], u.FSM)
	
	fsm.process(1)
	assert_eq(fsm._current_state, "s3")

# Test following FSM
# (s1 -> (s2 -> s3) -> s4)
func test_inner_switch_and_out():
	fsm.add_state(s1)
	fsm.default_state = "s1"
	
	fsm.add_state(innerFsm)
	innerFsm.add_state(s2)
	innerFsm.add_state(s3)
	innerFsm.default_state = "s2"
	
	fsm.add_state(s4)
	
	s1.add_transition("inner", tTrans)
	s2.add_transition("s3", tTrans)
	innerFsm.add_transition("s4", tTrans)
	
	fsm.enter(null)
	assert_eq(fsm._current_state, "s1")
	
	fsm.process(1)
	assert_eq(fsm._current_state, "inner")
	assert_eq(fsm._states["inner"]._current_state, 's2')
	
	fsm.process(1)
	assert_eq(fsm._current_state, "s4")
	
