extends "res://addons/gut/test.gd"
var u = preload("res://test/utils.gd").new()

var fsm
var state
var pseudo_node = {}

# #############
# Seutp/Teardown
# #############

func setup():
	pseudo_node = {
		enter = 0,
		process = 0,
		exit = 0
	}
	fsm = u.FSM.new_root("root", pseudo_node)
	state = TestState.new()
	state.name = "test"
	fsm.add_state(state)
	fsm.enter(null)

func teardown():
	fsm = null
	state = null
	pseudo_node = null
	
# #############
# Tests
# #############

func test_class_type():
	assert_extends(state, u.State)
	assert_extends(state, TestState)


func test_root():
	assert_ne(fsm.root, null)
	assert_has(fsm.root, "ok", "root is dict, dynamic creation of key in enter()")

func test_enter_process_simple():
	assert_eq(fsm.root["enter"], 1, "enter should be called once here")

	fsm.process(1)
	assert_eq(fsm.root["enter"], 1, "enter stil should be 1 since no state change happend")
	assert_eq(fsm.root["process"], 1, "root process should be called once")
	
	fsm.process(1)
	assert_eq(fsm.root["enter"], 1, "enter stil should be 1 since no state change happend")
	assert_eq(fsm.root["process"], 2, "root process should be called twice")

class TestState:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	func enter(from):
		root["enter"] += 1
		root["ok"] = true
	func process(delta):
		root["process"] += delta
	func exit(from):
		root["exit"] += 1

