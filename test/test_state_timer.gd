extends "res://addons/gut/test.gd"

var utils = preload("res://test/utils.gd").new()

var f
var s1
var s2
var trans
var false_trans

func setup():
	f = utils.FSM.new_root("root", null)
	s1 = utils.State.new()
	s1.name = "state_a"
	s2 = utils.State.new()
	s2.name = "state_b"
	
	f.add_state(s1)
	f.add_state(s2)
	
	trans = utils.ChangeTransition.new(false, 2)
	false_trans = utils.FalseTransition.new()
	
	s1.add_transition("state_b", trans)
	s2.add_transition("state_a", false_trans)
	f.default_state = "state_a"
	f.enter(null)

func teardown():
	f = null
	s1 = null
	s2 = null
	trans = null
	false_trans = null

func test_init_timer():
	assert_eq(f.state_time, 0, "Timer should start at 0.")

func test_change_state_and_timer():
	f.process(2)
	assert_eq(f._current_state, "state_a", "Wrong state.")
	assert_eq(f.state_time, 2, "Wrong state time. Should be 2.")
	
	f.process(3)
	assert_eq(f._current_state, "state_a", "Wrong state.")
	assert_eq(f.state_time, 5, "Wrong state time. Should be 5.")
	
	
	f.process(0)
	assert_eq(f._current_state, "state_b", "Wrong state.")
	assert_eq(f.state_time, 0, "Wrong state time. Should be 0.")
	
	f.process(2)
	assert_eq(f._current_state, "state_b", "Wrong state.")
	assert_eq(f.state_time, 2, "Wrong state time. Should be 2.")
	
	f.process(5.5)
	assert_eq(f._current_state, "state_b", "Wrong state.")
	assert_eq(f.state_time, 7.5, "Wrong state time. Should be 7.5.")
